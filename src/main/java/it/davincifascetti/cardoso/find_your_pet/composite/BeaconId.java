package it.davincifascetti.cardoso.find_your_pet.composite;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;

import org.springframework.lang.NonNull;

@Embeddable
public class BeaconId implements Serializable {
    @Column
    @GeneratedValue(generator = "system-uuid")
    private String uuid;

    @NonNull
    @Column(nullable = false, length = 64)
    private String fkPetName;

    @NonNull
    @Column(nullable = false)
    private Long fkPetProfile;

    public BeaconId() {
    }

    public BeaconId(String petName, Long petProfile) {
        this.fkPetName = petName;
        this.fkPetProfile = petProfile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        BeaconId accountId = (BeaconId) o;
        return uuid.equals(accountId.uuid) && fkPetName.equals(accountId.fkPetName)
                && fkPetProfile.equals(accountId.fkPetProfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, fkPetName, fkPetProfile);
    }
}
