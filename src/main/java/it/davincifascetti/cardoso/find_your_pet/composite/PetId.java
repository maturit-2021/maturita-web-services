package it.davincifascetti.cardoso.find_your_pet.composite;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.springframework.lang.NonNull;

@Embeddable
public class PetId implements Serializable {
    @NonNull
    @Column(nullable = false, length = 64)
    private String name;

    @NonNull
    @Column(nullable = false)
    private Long fkProfile;

    public PetId() {
    }

    public PetId(String name, Long fkProfile) {
        this.name = name;
        this.fkProfile = fkProfile;
    }

    public PetId(String name, int fkProfile) {
        this.name = name;
        this.fkProfile = Long.valueOf(fkProfile);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        PetId accountId = (PetId) o;
        return name.equals(accountId.name) && fkProfile.equals(accountId.fkProfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, fkProfile);
    }

    public String getName() {
        return this.name;
    }

    public Long getFkProfile() {
        return this.fkProfile;
    }
}
