package it.davincifascetti.cardoso.find_your_pet.composite;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.lang.NonNull;

@Embeddable
public class SightingId implements Serializable {
    @NonNull
    @Column(nullable = false)
    private String fkLostPet;

    @CreationTimestamp
    @Column(columnDefinition = "DATETIME default (NOW())")
    private Date creationDate;

    public SightingId() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        SightingId accountId = (SightingId) o;
        return fkLostPet.equals(accountId.fkLostPet) && creationDate.equals(accountId.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fkLostPet, creationDate);
    }
}
