package it.davincifascetti.cardoso.find_your_pet.composite;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.lang.NonNull;

@Embeddable
public class ChatId implements Serializable {
    @NonNull
    @Column(nullable = false)
    private Long fkProfileFrom;

    @NonNull
    @Column(nullable = false)
    private Long fkProfileTo;

    @CreationTimestamp
    @Column(columnDefinition = "DATETIME default (NOW())")
    private Date creationDate;

    public ChatId() {

    }

    public ChatId(Long from, Long to) {
        this.fkProfileFrom = from;
        this.fkProfileTo = to;
        this.creationDate = new Date();
    }

    public ChatId(int from, int to) {
        this.fkProfileFrom = Long.valueOf(from);
        this.fkProfileTo = Long.valueOf(to);
        this.creationDate = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        ChatId accountId = (ChatId) o;
        return fkProfileFrom.equals(accountId.fkProfileFrom) && fkProfileTo.equals(accountId.fkProfileTo)
                && creationDate.equals(accountId.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fkProfileFrom, fkProfileTo, creationDate);
    }
}
