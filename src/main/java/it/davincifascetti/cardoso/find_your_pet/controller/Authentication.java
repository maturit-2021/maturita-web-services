package it.davincifascetti.cardoso.find_your_pet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import it.davincifascetti.cardoso.find_your_pet.dto.AuthRegister;
import it.davincifascetti.cardoso.find_your_pet.dto.HttpResponse;
import it.davincifascetti.cardoso.find_your_pet.dto.JwtUser;
import it.davincifascetti.cardoso.find_your_pet.dto.LoginParams;
import it.davincifascetti.cardoso.find_your_pet.entity.UserEntity;
import it.davincifascetti.cardoso.find_your_pet.exceptions.NotFoundException;
import it.davincifascetti.cardoso.find_your_pet.service.AutenticationService;
import it.davincifascetti.cardoso.find_your_pet.utils.JwtTokenUtil;

@RestController
@RequestMapping("/auth")
public class Authentication {
    @Autowired
    private JwtTokenUtil jwtToken;

    @Autowired
    private AutenticationService profileService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@ModelAttribute LoginParams params) throws Exception {
        UserEntity user = profileService.findUser(params);
        
        if(user == null)
            return HttpResponse.error(new NotFoundException("User not found"));
        return ResponseEntity.ok(jwtToken.generateForUser(user));
    }

    @PostMapping("/refresh")
    public ResponseEntity<?> refresh() throws Exception {
        JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.ok(userDetails);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@ModelAttribute AuthRegister params) throws Exception {
        UserEntity user = profileService.register(params);
        return ResponseEntity.ok(jwtToken.generateForUser(user));
    }
}
