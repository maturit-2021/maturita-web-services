package it.davincifascetti.cardoso.find_your_pet.controller;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.davincifascetti.cardoso.find_your_pet.dto.HttpResponse;
import it.davincifascetti.cardoso.find_your_pet.dto.JwtUser;
import it.davincifascetti.cardoso.find_your_pet.dto.PostAddParams;
import it.davincifascetti.cardoso.find_your_pet.entity.PostEntity;
import it.davincifascetti.cardoso.find_your_pet.entity.ProfileEntity;
import it.davincifascetti.cardoso.find_your_pet.repository.PostRepository;
import it.davincifascetti.cardoso.find_your_pet.repository.ProfileRepository;

@RestController
@RequestMapping("/post")
public class Post {
    @Autowired
    private PostRepository postRep;

    @Autowired
    private ProfileRepository profileRep;

    @PostMapping("/add")
    public ResponseEntity<?> insertPost(@ModelAttribute PostAddParams params) throws Exception {
        JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<ProfileEntity> profile = profileRep.findById(user.getProfileId());
        
        PostEntity post =  new PostEntity();
        post.setProfile(profile.get());
        post.setContent(params.getContent());
        post.setImage(params.getImage().getBytes());

        return HttpResponse.ok(postRep.save(post).getId());
    }
}
