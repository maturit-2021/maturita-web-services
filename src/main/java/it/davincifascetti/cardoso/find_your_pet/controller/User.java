package it.davincifascetti.cardoso.find_your_pet.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import it.davincifascetti.cardoso.find_your_pet.dto.HttpResponse;
import it.davincifascetti.cardoso.find_your_pet.dto.JwtUser;
import it.davincifascetti.cardoso.find_your_pet.entity.ProfileEntity;
import it.davincifascetti.cardoso.find_your_pet.exceptions.NotFoundException;
import it.davincifascetti.cardoso.find_your_pet.repository.ProfileRepository;


@RestController
@RequestMapping("/user")
public class User {
    @Autowired
    private ProfileRepository profileRepository;
    
    @GetMapping("/me")
    public ResponseEntity<?> getSelfInformation() throws Exception {
        JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<ProfileEntity> profile = profileRepository.findById(userDetails.getProfileId());

        if(profile.isPresent())
            return HttpResponse.ok(profile);
            
        return HttpResponse.error(new NotFoundException("User not found"));
    }
}
