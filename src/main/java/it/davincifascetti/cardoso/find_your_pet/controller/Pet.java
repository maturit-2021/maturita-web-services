package it.davincifascetti.cardoso.find_your_pet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.davincifascetti.cardoso.find_your_pet.dto.HttpResponse;
import it.davincifascetti.cardoso.find_your_pet.dto.JwtUser;
import it.davincifascetti.cardoso.find_your_pet.entity.PetEntity;
import it.davincifascetti.cardoso.find_your_pet.repository.PetRepository;

@RestController
@RequestMapping("/pet")
public class Pet {
    @Autowired
    private PetRepository petRepository;

    @GetMapping("/own-pets")
    public ResponseEntity<?> getUserPets() throws Exception {
        JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<PetEntity> pets = petRepository.findByIdFkProfile(user.getProfileId());

        return HttpResponse.ok(pets);
    }
}
