package it.davincifascetti.cardoso.find_your_pet.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

import it.davincifascetti.cardoso.find_your_pet.composite.BeaconId;

@Entity
@Table(name = "beacons")
public class BeaconEntity {
    @EmbeddedId
    private BeaconId id;

    public BeaconEntity() {
    }

    public BeaconEntity(BeaconId id) {
        this.id = id;
    }

    public BeaconEntity(String petName, Long petProfile) {
        BeaconId id = new BeaconId(petName, petProfile);
        this.id = id;

    }

}
