package it.davincifascetti.cardoso.find_your_pet.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

import it.davincifascetti.cardoso.find_your_pet.composite.ChatId;

@Entity
@Table(name = "chats")
public class ChatEntity {
    @EmbeddedId
    private ChatId id;

    @NonNull
    @Column(nullable = false, columnDefinition = "TEXT")
    private String message;

    @NonNull
    @Column(nullable = false)
    private String messageType;

    public ChatEntity() {
    }

    public ChatEntity(ChatId id, String message, String messageType) {
        this.id = id;
        this.message = message;
        this.messageType = messageType;
    }

    public ChatEntity(Long from, Long to, String message, String messageType) {
        ChatId id = new ChatId(from, to);
        this.id = id;
        this.message = message;
        this.messageType = messageType;

    }

}
