package it.davincifascetti.cardoso.find_your_pet.entity;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.lang.NonNull;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Table(name = "comments")
public class CommentEntity {
    @Id
    @Column(length = 64)
    private UUID id = UUID.randomUUID();

    @Column(nullable = false)
    private String comment;

    @CreationTimestamp
    @Column(columnDefinition = "DATETIME default (NOW())")
    private Date creationDate;

    @NonNull
    @OneToOne
    @JoinColumn(name = "fkProfile", referencedColumnName = "id")
    private ProfileEntity profile;

    @NonNull
    @OneToOne
    @JoinColumn(name = "fkPost", referencedColumnName = "id")
    private PostEntity post;

    public CommentEntity() {
    }

    public CommentEntity(String comment) {
        this.comment = comment;
    }
}
