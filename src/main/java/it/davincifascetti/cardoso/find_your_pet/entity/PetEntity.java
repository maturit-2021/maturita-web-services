package it.davincifascetti.cardoso.find_your_pet.entity;

import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import it.davincifascetti.cardoso.find_your_pet.composite.PetId;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
@Table(name = "pets")
public class PetEntity {
    @EmbeddedId
    private PetId id;

    @CreationTimestamp
    @Column(columnDefinition = "DATE default (CURDATE())")
    private Date creationDate;

    @ManyToOne
    @JoinColumn(name = "fkProfile", referencedColumnName = "id", insertable = false, updatable = false)
    private ProfileEntity profile;

    public PetEntity() {
    }

    public PetEntity(PetId id) {
        this.id = id;
    }

    public PetEntity(String name, ProfileEntity profile) {
        this.id = new PetId(name, profile.getId());
    }

    public PetEntity(String name, Long profileId) {
        this.id = new PetId(name, profileId);
    }

    public PetEntity(String name, int profileId) {
        this.id = new PetId(name, profileId);
    }

    @Override
    public String toString() {
        return String.format("Pet[name=%d, date='%s', profile='%s']", this.id.getName(), creationDate, profile);
    }

    public void setId(PetId id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.creationDate = date;
    }
}
