package it.davincifascetti.cardoso.find_your_pet.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.geo.Point;
import org.springframework.lang.NonNull;

import it.davincifascetti.cardoso.find_your_pet.composite.SightingId;

@Entity
@Table(name = "sighting")
public class SightingEntity {
    @EmbeddedId
    private SightingId id;

    @NonNull
    @Column(name = "location", columnDefinition = "Point", nullable = false)
    private Point location;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "fkLostPet", referencedColumnName = "id", insertable = false, updatable = false)
    private LostPetEntity pet;

    @ManyToOne
    @JoinColumn(name = "fkProfile", referencedColumnName = "id")
    private ProfileEntity profile;

    public SightingEntity() {
    }

}
