package it.davincifascetti.cardoso.find_your_pet.entity;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.lang.NonNull;

@Entity
@Table(name = "posts")
public class PostEntity {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    @Column
    private String content;

    @Lob
    @Column
    private byte[] image;

    @CreationTimestamp
    @Column(columnDefinition = "DATETIME default (NOW())")
    private Date creationDate;

    @NonNull
    @OneToOne
    @JoinColumn(name = "fkProfile", referencedColumnName = "id", nullable = false)
    private ProfileEntity profile;

    @ManyToMany
    @JoinTable(name = "tags", joinColumns = { @JoinColumn(name = "fkPost") }, inverseJoinColumns = {
            @JoinColumn(name = "fkProfile") })
    Set<ProfileEntity> profiles = new HashSet<>();

    public PostEntity() {
    }

    public PostEntity(Long id, String content) {
        this.id = id;
        this.content = content;
    }

    public PostEntity(Long id, byte[] image) {
        this.id = id;
        this.image = image;
    }

    public PostEntity(Long id, String content, byte[] image) {
        this.id = id;
        this.content = content;
        this.image = image;
    }


    public Long getId() {
        return this.id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public byte[] getImage() {
        return this.image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public ProfileEntity getProfile() {
        return this.profile;
    }

    public void setProfile(ProfileEntity profile) {
        this.profile = profile;
    }

    public Set<ProfileEntity> getProfiles() {
        return this.profiles;
    }

    public void setProfiles(Set<ProfileEntity> profiles) {
        this.profiles = profiles;
    }
}
