package it.davincifascetti.cardoso.find_your_pet.entity;

import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.lang.NonNull;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Table(name = "lost_pets")
public class LostPetEntity {
    @Id
    @Column(length = 64)
    private UUID id = UUID.randomUUID();

    @CreationTimestamp
    @Column(columnDefinition = "DATETIME default (NOW())")
    private Date creationDate;

    @Column(nullable = false)
    private String status;

    @NonNull
    @ManyToOne
    @JoinColumns({ @JoinColumn(name = "fkPetName", referencedColumnName = "name", nullable = false),
            @JoinColumn(name = "fkPetProfile", referencedColumnName = "fkProfile", nullable = false) })
    private PetEntity pet;

    public LostPetEntity() {
    }
}
