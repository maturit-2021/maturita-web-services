package it.davincifascetti.cardoso.find_your_pet.exceptions;

public class NotFoundException extends HttpResponseException {
    public String code = "NOT_FOUND";

    public NotFoundException(String code, String message) {
        super(404, message);
        this.code = code;
    }

    public NotFoundException(String message) {
        super(404, message);
        this.message = message;
    }
}
