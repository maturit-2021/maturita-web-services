package it.davincifascetti.cardoso.find_your_pet.exceptions;

public abstract class HttpResponseException {
    public int status;
    public String message;

    public HttpResponseException(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public HttpResponseException(int status){
        this.status = status;
    }
}
