package it.davincifascetti.cardoso.find_your_pet.dto;

public class JwtUser {
    private Long id;
    private String email;
    private Long profile;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) throws Exception {
        if (id == null) {
            throw new Exception("Invalid user ID");
        }
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) throws Exception {
        if (email == null || email.equals("")) {
            throw new Exception("Invalid user email");
        }
        this.email = email;
    }

    public Long getProfileId() {
        return this.profile;
    }

    public void setProfile(Long profile) throws Exception {
        if (profile == null) {
            throw new Exception("Invalid user profile ID");
        }
        this.profile = profile;
    }

    @Override
    public String toString() {
        return "{" + " id='" + getId() + "'" + ", email='" + getEmail() + "'" + ", profile='" + getProfileId() + "'"
                + "}";
    }
}
