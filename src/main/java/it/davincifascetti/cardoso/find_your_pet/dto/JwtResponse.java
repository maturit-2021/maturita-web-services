package it.davincifascetti.cardoso.find_your_pet.dto;

import java.io.Serializable;

public class JwtResponse implements Serializable{
    private String token;
    private String refreshToken;
    private String email;

    

    public JwtResponse(String token) {
        this.token = token;
    }

    public JwtResponse(String token, String email, String refreshToken) {
        this.token = token;
        this.email = email;
        this.refreshToken = refreshToken;
    }

    public String getToken() {
        return this.token;
    }

    public String getEmail() {
        return this.email;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    @Override
    public String toString() {
        return "{" +
            " token='" + getToken() + "'" +
            ", refreshToken='" + getRefreshToken() + "'" +
            ", email='" + getEmail() + "'" +
            "}";
    }


}
