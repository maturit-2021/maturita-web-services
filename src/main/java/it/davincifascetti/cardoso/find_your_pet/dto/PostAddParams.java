package it.davincifascetti.cardoso.find_your_pet.dto;


import org.springframework.web.multipart.MultipartFile;

public class PostAddParams {
    private String content;
    private MultipartFile image;


    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MultipartFile getImage() {
        return this.image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

}
