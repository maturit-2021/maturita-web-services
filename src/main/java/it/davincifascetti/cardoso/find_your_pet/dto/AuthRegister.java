package it.davincifascetti.cardoso.find_your_pet.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AuthRegister {
    @Email
    @NotNull(message = "Email cannot be null")
    @NotEmpty
    private String email;

    @NotNull
    @Size(max = 128, message = "Password cannot have more then 128 characters")
    private String password;

    @NotNull
    private String name;

    @NotNull
    private String city;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
