package it.davincifascetti.cardoso.find_your_pet.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.lang.Nullable;

import it.davincifascetti.cardoso.find_your_pet.exceptions.HttpResponseException;

@JsonSerialize(using = HttpResponseSerializer.class)
public class HttpResponse<T> {
    public T data;

    public HttpResponse(T body){
        this.data = body;
    }


    public static BodyBuilder ok(){
        return ResponseEntity.ok();
    }

    public static <T> ResponseEntity<HttpResponse<T>> ok(@Nullable T body) {
        return ok().body(new HttpResponse<T>(body));
    }

    public static <T extends HttpResponseException> ResponseEntity<T> error(Class<T> error) throws Exception {
        T e = error.getDeclaredConstructor().newInstance();
        return ResponseEntity.status(e.status).body(e);
    }

    public static <T extends HttpResponseException> ResponseEntity<T> error(T error) {
        return ResponseEntity.status(error.status).body(error);
    }
}
