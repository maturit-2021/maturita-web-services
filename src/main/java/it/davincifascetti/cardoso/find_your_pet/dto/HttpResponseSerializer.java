package it.davincifascetti.cardoso.find_your_pet.dto;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class HttpResponseSerializer extends StdSerializer<HttpResponse<?>> {

    public HttpResponseSerializer() {
        this(null);
    }

    public HttpResponseSerializer(Class<HttpResponse<?>> t) {
        super(t);
    }

    @Override
    public void serialize(HttpResponse<?> value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartObject();
        jgen.writeObjectField("data", value.data);
        
        if(value.data != null && (value.data instanceof List)){
            jgen.writeNumberField("length", ((List<?>)value.data).size());
        }

        jgen.writeEndObject();
    }
}
