package it.davincifascetti.cardoso.find_your_pet.repository;

import org.springframework.data.repository.CrudRepository;

import it.davincifascetti.cardoso.find_your_pet.composite.ChatId;
import it.davincifascetti.cardoso.find_your_pet.entity.ChatEntity;

public interface ChatRepository extends CrudRepository<ChatEntity, ChatId> {

}
