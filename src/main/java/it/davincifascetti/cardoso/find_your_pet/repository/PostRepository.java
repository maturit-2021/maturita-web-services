package it.davincifascetti.cardoso.find_your_pet.repository;

import org.springframework.data.repository.CrudRepository;

import it.davincifascetti.cardoso.find_your_pet.entity.PostEntity;

public interface PostRepository extends CrudRepository<PostEntity, Long> {

}
