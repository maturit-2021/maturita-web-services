package it.davincifascetti.cardoso.find_your_pet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.davincifascetti.cardoso.find_your_pet.entity.UserEntity;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByEmailAndPassword(String email, String password);
}
