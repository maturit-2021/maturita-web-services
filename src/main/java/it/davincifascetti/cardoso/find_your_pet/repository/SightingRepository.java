package it.davincifascetti.cardoso.find_your_pet.repository;

import org.springframework.data.repository.CrudRepository;

import it.davincifascetti.cardoso.find_your_pet.composite.SightingId;
import it.davincifascetti.cardoso.find_your_pet.entity.SightingEntity;

public interface SightingRepository extends CrudRepository<SightingEntity, SightingId> {

}
