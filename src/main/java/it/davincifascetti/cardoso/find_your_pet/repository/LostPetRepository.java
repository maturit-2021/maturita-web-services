package it.davincifascetti.cardoso.find_your_pet.repository;

import org.springframework.data.repository.CrudRepository;

import it.davincifascetti.cardoso.find_your_pet.entity.LostPetEntity;

public interface LostPetRepository extends CrudRepository<LostPetEntity, String> {

}
