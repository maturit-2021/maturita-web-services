package it.davincifascetti.cardoso.find_your_pet.repository;

import org.springframework.data.repository.CrudRepository;

import it.davincifascetti.cardoso.find_your_pet.composite.BeaconId;
import it.davincifascetti.cardoso.find_your_pet.entity.BeaconEntity;

public interface BeaconRepository extends CrudRepository<BeaconEntity, BeaconId> {

}
