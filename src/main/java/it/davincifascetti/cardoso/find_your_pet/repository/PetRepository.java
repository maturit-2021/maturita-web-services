package it.davincifascetti.cardoso.find_your_pet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.davincifascetti.cardoso.find_your_pet.composite.PetId;
import it.davincifascetti.cardoso.find_your_pet.entity.PetEntity;

@Repository
public interface PetRepository extends CrudRepository<PetEntity, PetId> {
    List<PetEntity> findByIdFkProfile(Long profileId);
}
