package it.davincifascetti.cardoso.find_your_pet.repository;

import org.springframework.data.repository.CrudRepository;

import it.davincifascetti.cardoso.find_your_pet.entity.CommentEntity;

public interface CommentRepository extends CrudRepository<CommentEntity, String> {

}
