package it.davincifascetti.cardoso.find_your_pet.service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import it.davincifascetti.cardoso.find_your_pet.dto.AuthRegister;
import it.davincifascetti.cardoso.find_your_pet.dto.HttpResponse;
import it.davincifascetti.cardoso.find_your_pet.dto.LoginParams;
import it.davincifascetti.cardoso.find_your_pet.entity.ProfileEntity;
import it.davincifascetti.cardoso.find_your_pet.entity.UserEntity;
import it.davincifascetti.cardoso.find_your_pet.exceptions.HttpResponseException;
import it.davincifascetti.cardoso.find_your_pet.exceptions.NotFoundException;
import it.davincifascetti.cardoso.find_your_pet.repository.ProfileRepository;
import it.davincifascetti.cardoso.find_your_pet.repository.UserRepository;

@Service
@Transactional(rollbackOn = Exception.class)
public class AutenticationService {
    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private UserRepository userRepository;

    public UserEntity register(AuthRegister params) throws Exception {
        try{
            String password = hashPassword(params.getPassword());
            ProfileEntity profile = new ProfileEntity(params.getName(), params.getCity());
            UserEntity user = new UserEntity(profile, params.getEmail(), password);

            profileRepository.save(profile);
            userRepository.save(user);

            return user;
        }catch(Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public UserEntity findUser(LoginParams params) throws Exception {
        String password = hashPassword(params.getPassword());
        UserEntity user = userRepository.findByEmailAndPassword(params.getEmail(), password);

        return user;
    }

    private String hashPassword(String password) throws Exception{
        // MessageDigest md = MessageDigest.getInstance("SHA-256");
        // return md.digest(password.getBytes(StandardCharsets.UTF_8)).toString();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] byteOfTextToHash = password.getBytes(StandardCharsets.UTF_8);
        byte[] hashedByetArray = digest.digest(byteOfTextToHash);
        String encoded = Base64.getEncoder().encodeToString(hashedByetArray);

        return encoded;
    }
}
