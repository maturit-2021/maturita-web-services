package it.davincifascetti.cardoso.find_your_pet.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import it.davincifascetti.cardoso.find_your_pet.entity.UserEntity;
import it.davincifascetti.cardoso.find_your_pet.dto.JwtResponse;
import it.davincifascetti.cardoso.find_your_pet.dto.JwtUser;

// https://italiancoders.it/autenticazione-di-servizi-rest-con-jwt-spring/

@Component
public class JwtTokenUtil implements Serializable {
    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.duration}")
    private Long duration;

    @Value("${jwt.refresh_duration}")
    private int refreshDuration;

    @Autowired
    private JwtTokenUtil jwtToken;

    public String getUsernameFromToken(String token) {
        String username;
        try {
            final Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public JwtUser getUserFromToken(String token) {
        if (token == null)
            return null;

        try {
            final Claims claims = getClaimsFromToken(token);
            JwtUser user = new JwtUser();
            Object id = claims.getOrDefault("id", null);
            Object email = claims.getOrDefault("email", null);
            Object profileId = claims.getOrDefault("profileId", null);

            user.setId(id != null ? Long.valueOf(id.toString()) : null);
            user.setEmail(email != null ? email.toString() : null);
            user.setProfile(profileId != null ? Long.valueOf(profileId.toString()) : null);
            return user;
        } catch (Exception e) {
            return null;
        }

    }

    public Date getCreatedDateFromToken(String token) {
        Date created;
        try {
            final Claims claims = getClaimsFromToken(token);
            created = new Date((Long) claims.get("createdAt"));
        } catch (Exception e) {
            created = null;
        }
        return created;
    }

    public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }

    /**
     * Retrieve the the claim content from the given token
     */
    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            claims = null;
        }
        return claims;
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateToken(UserEntity user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("id", user.getId());
        claims.put("email", user.getEmail());
        claims.put("profileId", user.getProfile().getId());
        claims.put("createdAt", new Date());

        return buildToken(claims, 0);
    }

    // Build a token with the given claims
    String buildToken(Map<String, Object> claims, int additionalDuration) {
        Date expiration = new Date(System.currentTimeMillis() + additionalDuration + duration * 1000);

        return Jwts
            .builder()
            .setClaims(claims)
            .setExpiration(expiration)
            .signWith(SignatureAlgorithm.HS256, secret)
            .compact();
    }

    public Boolean canTokenBeRefreshed(String token) {
        // final Date created = getCreatedDateFromToken(token);
        return !isTokenExpired(token);
    }

    public String refreshToken(String token) {
        String refreshedToken;
        try {
            final Claims claims = getClaimsFromToken(token);
            claims.put("createdAt", new Date());

            refreshedToken = buildToken(claims, refreshDuration);
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    public Boolean validateToken(String token, JwtUser userDetails) {
        // JwtUser user = (JwtUser) userDetails;
        // final String username = getUsernameFromToken(token);
        return !isTokenExpired(token);
    }

    /**
     * Generates a JWT token and a Refresh token for the given user
     * @param user
     * @return
     */
    public JwtResponse generateForUser(UserEntity user){
        String token = jwtToken.generateToken(user);
        String refreshToken = jwtToken.refreshToken(token);
        JwtResponse response = new JwtResponse(token, user.getEmail(), refreshToken);

        return response;
    }
}