# First stage, build the jar file
FROM maven:latest as builder

#Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./src ./src
COPY ./pom.xml ./

RUN mvn package

# Secon stage, creates the image for production
FROM adoptopenjdk:11-jre-hotspot

ENV ENVIRONMENT production

# RUN addgroup --system spring && adduser --system spring --group spring
# USER spring:spring

#Create app directory
# RUN chown spring:spring  /usr/src/app
WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/target ./


EXPOSE 8080
ENTRYPOINT ["java","-jar","FindYourPet.jar"]